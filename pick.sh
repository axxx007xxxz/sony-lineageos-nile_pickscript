#!/bin/bash

cd bionic/; git fetch github 02ced6dc860f5e7653b199eb9fde6fce063d6990; git cherry-pick FETCH_HEAD; cd ../
cd build/make/; git fetch github 5a44beafca26ad2169ee3bbb193ad43bfcf26864; git cherry-pick FETCH_HEAD; cd ../../
cd hardware/sony/macaddrsetup/; git fetch github refs/changes/17/210917/1; git checkout FETCH_HEAD; cd ../../../